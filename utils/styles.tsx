import { Global } from '@emotion/react';
export const cssGlobalStyle = (
  <Global
    styles={{
      body: {
        padding: 0,
        margin: 0,
        '*': {
          fontFamily: 'inter',
        },
      },
    }}
  />
);
