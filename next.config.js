const withGraphQL = require('next-plugin-graphql');

module.exports = withGraphQL({
  reactStrictMode: true,
  images: {
    domains: ['squeued.vercel.app'],
  },
});
