import Document, { Html, Head, Main, NextScript } from 'next/document';
import { image } from '../app/assets';

class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
          <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
          <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
          <link rel="manifest" href="/site.webmanifest" />
          <meta name="msapplication-TileColor" content="#da532c" />
          <meta name="theme-color" content="#ffffff" />

          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin="" />
          <link
            href="https://fonts.googleapis.com/css2?family=Inter:wght@100;200;300;400;500;600;700&display=swap"
            rel="stylesheet"
          />
          <title>Squeued - Ngantri dengan cara baru</title>
          <meta name="title" content="Squeued - Ngantri dengan cara baru" />
          <meta
            name="description"
            content="Squeued, kami hadir memberikan solusi antrian online untuk kebutuhan mengantri anda sehari - hari. Anda cukup melakukan booking di rumah dan datang sesuai dengan nomor dan estimasi waktu yang telah kami berikan. Ngantri dengan squeued memberikan kemudahan dan cara baru dalam mengantri"
          />
          <meta property="og:type" content="website" />
          <meta property="og:url" content={process.env.NEXT_PUBLIC_URL} />
          <meta property="og:title" content="Squeued - Ngantri dengan cara baru" />
          <meta
            property="og:description"
            content="Squeued, kami hadir memberikan solusi antrian online untuk kebutuhan mengantri anda sehari - hari. Anda cukup melakukan booking di rumah dan datang sesuai dengan nomor dan estimasi waktu yang telah kami berikan. Ngantri dengan squeued memberikan kemudahan dan cara baru dalam mengantri"
          />
          <meta property="og:image" content={image} />
          <meta property="twitter:card" content="summary_large_image" />
          <meta property="twitter:url" content={process.env.NEXT_PUBLIC_URL} />
          <meta property="twitter:title" content="Squeued - Ngantri dengan cara baru" />
          <meta
            property="twitter:description"
            content="Squeued, kami hadir memberikan solusi antrian online untuk kebutuhan mengantri anda sehari - hari. Anda cukup melakukan booking di rumah dan datang sesuai dengan nomor dan estimasi waktu yang telah kami berikan. Ngantri dengan squeued memberikan kemudahan dan cara baru dalam mengantri"
          />
          <meta property="twitter:image" content={image} />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
