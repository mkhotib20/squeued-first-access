// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import fs from 'fs';
import type { NextApiRequest, NextApiResponse } from 'next';

type Data = {
  ip?: string;
};

export default function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  const { body } = req;
  const ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
  res.json({ ip: ip?.toString() });
}
