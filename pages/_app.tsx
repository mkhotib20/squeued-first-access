import { ApolloProvider } from '@apollo/client';
import type { AppProps } from 'next/app';
import { apolloClient } from '../app/utils/apollo';
import { cssGlobalStyle } from '../utils/styles';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      {cssGlobalStyle}
      <ApolloProvider client={apolloClient}>
        <Component {...pageProps} />
      </ApolloProvider>
    </>
  );
}
export default MyApp;
