export const getGeo = (): Promise<any> => {
  return new Promise(async (resolve) => {
    const result = await navigator.permissions.query({ name: 'geolocation' });
    if (result.state == 'granted') {
      navigator.geolocation.getCurrentPosition(
        (res) => {
          const { latitude, longitude } = res.coords;
          resolve({ latitude, longitude });
        },
        () => resolve(undefined),
        { enableHighAccuracy: true }
      );
    } else if (result.state == 'prompt') {
      navigator.geolocation.getCurrentPosition(
        (res) => {
          const { latitude, longitude } = res.coords;
          resolve({ latitude, longitude });
        },
        (err) => {
          resolve(undefined);
          console.log(err);
        },
        { enableHighAccuracy: true }
      );
    } else if (result.state == 'denied') {
      resolve(undefined);
    }
    result.onchange = function () {
      navigator.geolocation.getCurrentPosition(
        (res) => {
          const { latitude, longitude } = res.coords;
          resolve({ latitude, longitude });
        },
        () => resolve(undefined),
        { enableHighAccuracy: true }
      );
    };
  });
};
