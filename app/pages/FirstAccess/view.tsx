import { useMutation } from '@apollo/client';
import { NextPage } from 'next';
import Head from 'next/head';
import Image from 'next/image';
import React, { useEffect, useRef, useState } from 'react';
import { image } from '../../assets';
import Loading from '../../components/Loading';
import { getGeo } from '../../helpers/geolocation';
import mutationSubscribe from './queries/subscribe.gql';
import mutationVisit from './queries/visit.gql';
import { cssButton, cssDescription, cssInput, cssInputMail, cssMainTitle, cssMainWrapper } from './styles';

const context = {
  headers: {
    Authorization: `Bearer ${process.env.NEXT_PUBLIC_GRAPHCMS_TOKEN}`,
  },
};

const ComponentIndex: NextPage = () => {
  const [email, setEmail] = useState('');
  const [kota, setKota] = useState('');
  const geolocation = useRef<any>();
  const [visit, { loading: visiting }] = useMutation(mutationVisit, {
    onError: (err) => console.error(err.message),
    onCompleted: () => {},
    context,
  });
  const handleFInal = () => {
    alert('Berhasil, kami akan mengirimkan email saat kami hadir di kota mu');
    window.location.reload();
  };
  const [subscribe, { loading: subscribing }] = useMutation(mutationSubscribe, {
    onError: handleFInal,
    onCompleted: handleFInal,
    context,
  });
  const loading = visiting || subscribing;

  const handleInit = async () => {
    const rsp = await fetch('/api/ip');
    const { ip } = await rsp.json();
    const geo = await getGeo();
    geolocation.current = geo;

    const variables = { ip, geo };
    visit({ variables });
  };
  useEffect(() => {
    handleInit();
  }, []);

  const handleSubmit: React.FormEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();

    if (!email) {
      alert('Masukan alamat email anda');
      return;
    }
    subscribe({
      variables: {
        email,
        geo: geolocation.current,
        kota,
      },
    });
  };

  return (
    <div style={{ width: '100vw', height: '100vh' }}>
      <Head>
        <title>Squeued - Ngantri dengan cara baru</title>
      </Head>
      <div css={cssMainWrapper}>
        <Image alt="Squeued Icon ngantri cara baru online" width={100} height={100} src={image} />
        <h4 css={cssMainTitle}>Hi, kami segera hadir. Beri tahu kami dimana anda tinggal</h4>
        <form
          onSubmit={handleSubmit}
          style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}
        >
          <input
            disabled={loading}
            value={email}
            onChange={(e) => setEmail(e.currentTarget.value)}
            autoFocus
            type="email"
            css={cssInputMail}
            placeholder="Email"
          />
          <input
            disabled={loading}
            value={kota}
            onChange={(e) => setKota(e.currentTarget.value)}
            autoFocus
            type="text"
            css={cssInputMail}
            placeholder="Kota Kamu (optional)"
          />
          <p css={cssInput}>Masukan email anda untuk mendapatkan kabar kapan kami akan hadir</p>
          <button disabled={loading} type="submit" css={cssButton}>
            {loading && <Loading />}
            Kirim
          </button>
        </form>
        <p css={cssDescription}>
          Squeued adalah platform antrian online untuk memberikan pengalaman baru dalam mengantri.
        </p>
      </div>
    </div>
  );
};

export default ComponentIndex;
