import dynamic from 'next/dynamic';

const IndexPage = dynamic(() => import('./view'), { ssr: true });

export default IndexPage;
