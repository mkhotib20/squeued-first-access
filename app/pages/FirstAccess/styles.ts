import { css } from '@emotion/react';

export const cssMainWrapper = css({
  display: 'flex',
  overflow: 'hidden',
  justifyContent: 'center',
  alignItems: 'center',
  height: 'calc(100% - 16px)',
  flexDirection: 'column',
  padding: '8px 16px',
});

export const cssMainTitle = css({
  fontSize: 30,
  fontWeight: 200,
  width: 700,
  maxWidth: '100%',
  textAlign: 'center',
  marginBottom: 0,
});

export const cssInput = css({
  textAlign: 'center',
  opacity: 0.5,
  fontSize: 11,
  width: 300,
  maxWidth: '100%',
});

export const cssInputMail = css({
  width: 300,
  padding: '10px',
  marginTop: 20,
  maxWidth: '100%',
  borderWidth: 0,
  borderBottomWidth: 1,
  '&:focus': {
    boxShadow: 'none',
    outline: 'none',
    borderBottomColor: '#0ECF83',
  },
});

export const cssButton = css({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  marginTop: 20,
  width: 200,
  padding: 10,
  backgroundColor: '#0ECF83',
  border: 'none',
  color: 'white',
});

export const cssDescription = css({
  opacity: 0.8,
  fontSize: 12,
  margin: 0,
  width: 700,
  maxWidth: '90%',
  position: 'absolute',
  bottom: 25,
  textAlign: 'center',
});
