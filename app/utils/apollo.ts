import { ApolloClient, InMemoryCache } from '@apollo/client';
import { GRAPHCMS_URL } from '../constants';

export const apolloClient = new ApolloClient({
  uri: GRAPHCMS_URL,
  cache: new InMemoryCache(),
});
