import dynamic from 'next/dynamic';

const Loading = dynamic(() => import('./view'), { ssr: false });

export default Loading;
