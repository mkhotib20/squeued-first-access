import React from 'react';
import { cssLoader } from './styles';

const ComponentLoading = () => <div css={cssLoader}></div>;

export default ComponentLoading;
