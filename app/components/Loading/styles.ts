import { css, keyframes } from '@emotion/react';

const spining = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

export const cssLoader = css`
  margin-right: 8px;
  border: 1px solid #f3f3f3; /* Light grey */
  border-top: 1px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 15px;
  height: 15px;
  animation: ${spining} 2s linear infinite;
`;
